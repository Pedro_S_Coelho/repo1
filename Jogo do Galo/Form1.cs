﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jogo_do_Galo
{
    public partial class Form1 : Form
    {

        double player;
        double scoringplayer;
        double scorep1;
        double scorep2;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            player = 1;
            scoringplayer = 0;
            scorep1 = 0;
            scorep2 = 0;
        }

        private void reset()
        {
            l1c1.Text = "?";
            l1c2.Text = "?";
            l1c3.Text = "?";
            l2c1.Text = "?";
            l2c2.Text = "?";
            l2c3.Text = "?";
            l3c1.Text = "?";
            l3c2.Text = "?";
            l3c3.Text = "?";
        }

        private void tcheka()
        {
            // 3 em Linha
            if (l1c1.Text == "X" && l1c2.Text == "X" && l1c3.Text == "X" || l1c1.Text == "O" && l1c2.Text == "O" && l1c3.Text == "O")
            {
                MessageBox.Show($"Player {scoringplayer} ganhou!", "Vencedor!");
                reset();
            }
            else if (l2c1.Text == "X" && l2c2.Text == "X" && l2c3.Text == "X" || l2c1.Text == "O" && l2c2.Text == "O" && l2c3.Text == "O")
            {
                MessageBox.Show($"Player {scoringplayer} ganhou!", "Vencedor!");
                reset();
            }
            else if (l3c1.Text == "X" && l3c2.Text == "X" && l3c3.Text == "X" || l3c1.Text == "O" && l3c2.Text == "O" && l3c3.Text == "O")
            {
                MessageBox.Show($"Player {scoringplayer} ganhou!", "Vencedor!");
                reset();
            }
            // 3 em Coluna
            else if (l1c1.Text == "X" && l2c1.Text == "X" && l3c1.Text == "X" || l1c1.Text == "O" && l2c1.Text == "O" && l3c1.Text == "O")
            {
                MessageBox.Show($"Player {scoringplayer} ganhou!", "Vencedor!");
                reset();
            }
            else if (l1c2.Text == "X" && l2c2.Text == "X" && l3c2.Text == "X" || l1c2.Text == "O" && l2c2.Text == "O" && l3c2.Text == "O")
            {
                MessageBox.Show($"Player {scoringplayer} ganhou!", "Vencedor!");
                reset();
            }
            else if (l1c3.Text == "X" && l2c3.Text == "X" && l3c3.Text == "X" || l1c3.Text == "O" && l2c3.Text == "O" && l3c3.Text == "O")
            {
                MessageBox.Show($"Player {scoringplayer} ganhou!", "Vencedor!");
                reset();
            }
            // 3 na Diagonal
            else if (l1c1.Text == "X" && l2c2.Text == "X" && l3c3.Text == "X" || l1c1.Text == "O" && l2c2.Text == "O" && l3c3.Text == "O")
            {
                MessageBox.Show($"Player {scoringplayer} ganhou!", "Vencedor!");
                reset();
            }
            else if (l1c3.Text == "X" && l2c2.Text == "X" && l3c1.Text == "X" || l3c1.Text == "O" && l2c2.Text == "O" && l1c3.Text == "O")
            {
                MessageBox.Show($"Player {scoringplayer} ganhou!", "Vencedor!");
                reset();
            }
            // Empate
            else if (l1c1.Text != "?" && l1c2.Text != "?" && l1c3.Text != "?" && l2c1.Text != "?" && l2c2.Text != "?" && l2c3.Text != "?" && l3c1.Text != "?" && l3c2.Text != "?" && l3c3.Text != "?")
            {
                MessageBox.Show("Os Players Empataram!", "Empate!");
                reset();
            }
        }

        private void l1c1_Click(object sender, EventArgs e)
        {
            if (player == 1 && l1c1.Text == "?")
            {
                l1c1.Text = "X";
                player = 2;
                scoringplayer = 1;
                playerplaying();
            }
            else if(player == 2 && l1c1.Text == "?")
            {
                l1c1.Text = "O";
                player = 1;
                scoringplayer = 2;
                playerplaying();
            }
            else
            {
                MessageBox.Show("Tente Novamente!","Jogada Inválida");
            }
            tcheka();
        }

        private void l1c2_Click(object sender, EventArgs e)
        {
            if (player == 1 && l1c2.Text == "?")
            {
                l1c2.Text = "X";
                player = 2;
                scoringplayer = 1;
                playerplaying();
            }
            else if (player == 2 && l1c2.Text == "?")
            {
                l1c2.Text = "O";
                player = 1;
                scoringplayer = 2;
                playerplaying();
            }
            else
            {
                MessageBox.Show("Tente Novamente!", "Jogada Inválida");
            }
            tcheka();
        }

        private void l1c3_Click(object sender, EventArgs e)
        {
            if (player == 1 && l1c3.Text == "?")
            {
                l1c3.Text = "X";
                player = 2;
                scoringplayer = 1;
                playerplaying();
            }
            else if (player == 2 && l1c3.Text == "?")
            {
                l1c3.Text = "O";
                player = 1;
                scoringplayer = 2;
                playerplaying();
            }
            else
            {
                MessageBox.Show("Tente Novamente!", "Jogada Inválida");
            }
            tcheka();
        }

        private void l2c1_Click(object sender, EventArgs e)
        {
            if (player == 1 && l2c1.Text == "?")
            {
                l2c1.Text = "X";
                player = 2;
                scoringplayer = 1;
                playerplaying();
            }
            else if (player == 2 && l2c1.Text == "?")
            {
                l2c1.Text = "O";
                player = 1;
                scoringplayer = 2;
                playerplaying();
            }
            else
            {
                MessageBox.Show("Tente Novamente!", "Jogada Inválida");
            }
            tcheka();
        }

        private void l2c2_Click(object sender, EventArgs e)
        {
            if (player == 1 && l2c2.Text == "?")
            {
                l2c2.Text = "X";
                player = 2;
                scoringplayer = 1;
                playerplaying();
            }
            else if (player == 2 && l2c2.Text == "?")
            {
                l2c2.Text = "O";
                player = 1;
                scoringplayer = 2;
                playerplaying();
            }
            else
            {
                MessageBox.Show("Tente Novamente!", "Jogada Inválida");
            }
            tcheka();
        }

        private void l2c3_Click(object sender, EventArgs e)
        {
            if (player == 1 && l2c3.Text == "?")
            {
                l2c3.Text = "X";
                player = 2;
                scoringplayer = 1;
                playerplaying();
            }
            else if (player == 2 && l2c3.Text == "?")
            {
                l2c3.Text = "O";
                player = 1;
                scoringplayer = 2;
                playerplaying();
            }
            else
            {
                MessageBox.Show("Tente Novamente!", "Jogada Inválida");
            }
            tcheka();
        }

        private void l3c1_Click(object sender, EventArgs e)
        {
            if (player == 1 && l3c1.Text == "?")
            {
                l3c1.Text = "X";
                player = 2;
                scoringplayer = 1;
                playerplaying();
            }
            else if (player == 2 && l3c1.Text == "?")
            {
                l3c1.Text = "O";
                player = 1;
                scoringplayer = 2;
                playerplaying();
            }
            else
            {
                MessageBox.Show("Tente Novamente!", "Jogada Inválida");
            }
            tcheka();
        }

        private void l3c2_Click(object sender, EventArgs e)
        {
            if (player == 1 && l3c2.Text == "?")
            {
                l3c2.Text = "X";
                player = 2;
                scoringplayer = 1;
                playerplaying();
            }
            else if (player == 2 && l3c2.Text == "?")
            {
                l3c2.Text = "O";
                player = 1;
                scoringplayer = 2;
                playerplaying();
            }
            else
            {
                MessageBox.Show("Tente Novamente!", "Jogada Inválida");
            }
            tcheka();
        }

        private void l3c3_Click(object sender, EventArgs e)
        {
            if (player == 1 && l3c3.Text == "?")
            {
                l3c3.Text = "X";
                player = 2;
                scoringplayer = 1;
                playerplaying();
            }
            else if (player == 2 && l3c3.Text == "?")
            {
                l3c3.Text = "O";
                player = 1;
                scoringplayer = 2;
                playerplaying();
            }
            else
            {
                MessageBox.Show("Tente Novamente!", "Jogada Inválida");
            }
            tcheka();
        }

        private void resetbtn_Click(object sender, EventArgs e)
        {
            reset();
        }

        private void fecharbtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void playerplaying()
        {
            if (player == 1)
            {
                textBox1.Text = "Player 1";
            }
            else
            {
                textBox1.Text = "Player 2";
            }
        }
    }
}
