﻿namespace Jogo_do_Galo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.l1c1 = new System.Windows.Forms.Button();
            this.l1c2 = new System.Windows.Forms.Button();
            this.l1c3 = new System.Windows.Forms.Button();
            this.l2c1 = new System.Windows.Forms.Button();
            this.l2c2 = new System.Windows.Forms.Button();
            this.l2c3 = new System.Windows.Forms.Button();
            this.l3c1 = new System.Windows.Forms.Button();
            this.l3c2 = new System.Windows.Forms.Button();
            this.l3c3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.player1 = new System.Windows.Forms.Label();
            this.player2 = new System.Windows.Forms.Label();
            this.score1 = new System.Windows.Forms.TextBox();
            this.score2 = new System.Windows.Forms.TextBox();
            this.resetbtn = new System.Windows.Forms.Button();
            this.fecharbtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // l1c1
            // 
            this.l1c1.Font = new System.Drawing.Font("Microsoft YaHei UI", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l1c1.Location = new System.Drawing.Point(127, 132);
            this.l1c1.Name = "l1c1";
            this.l1c1.Size = new System.Drawing.Size(101, 90);
            this.l1c1.TabIndex = 3;
            this.l1c1.Text = "?";
            this.l1c1.UseVisualStyleBackColor = true;
            this.l1c1.Click += new System.EventHandler(this.l1c1_Click);
            // 
            // l1c2
            // 
            this.l1c2.Font = new System.Drawing.Font("Microsoft YaHei UI", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l1c2.Location = new System.Drawing.Point(237, 132);
            this.l1c2.Name = "l1c2";
            this.l1c2.Size = new System.Drawing.Size(101, 90);
            this.l1c2.TabIndex = 4;
            this.l1c2.Text = "?";
            this.l1c2.UseVisualStyleBackColor = true;
            this.l1c2.Click += new System.EventHandler(this.l1c2_Click);
            // 
            // l1c3
            // 
            this.l1c3.Font = new System.Drawing.Font("Microsoft YaHei UI", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l1c3.Location = new System.Drawing.Point(347, 132);
            this.l1c3.Name = "l1c3";
            this.l1c3.Size = new System.Drawing.Size(101, 90);
            this.l1c3.TabIndex = 5;
            this.l1c3.Text = "?";
            this.l1c3.UseVisualStyleBackColor = true;
            this.l1c3.Click += new System.EventHandler(this.l1c3_Click);
            // 
            // l2c1
            // 
            this.l2c1.Font = new System.Drawing.Font("Microsoft YaHei UI", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l2c1.Location = new System.Drawing.Point(127, 237);
            this.l2c1.Name = "l2c1";
            this.l2c1.Size = new System.Drawing.Size(101, 90);
            this.l2c1.TabIndex = 6;
            this.l2c1.Text = "?";
            this.l2c1.UseVisualStyleBackColor = true;
            this.l2c1.Click += new System.EventHandler(this.l2c1_Click);
            // 
            // l2c2
            // 
            this.l2c2.Font = new System.Drawing.Font("Microsoft YaHei UI", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l2c2.Location = new System.Drawing.Point(237, 237);
            this.l2c2.Name = "l2c2";
            this.l2c2.Size = new System.Drawing.Size(101, 90);
            this.l2c2.TabIndex = 7;
            this.l2c2.Text = "?";
            this.l2c2.UseVisualStyleBackColor = true;
            this.l2c2.Click += new System.EventHandler(this.l2c2_Click);
            // 
            // l2c3
            // 
            this.l2c3.Font = new System.Drawing.Font("Microsoft YaHei UI", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l2c3.Location = new System.Drawing.Point(347, 237);
            this.l2c3.Name = "l2c3";
            this.l2c3.Size = new System.Drawing.Size(101, 90);
            this.l2c3.TabIndex = 8;
            this.l2c3.Text = "?";
            this.l2c3.UseVisualStyleBackColor = true;
            this.l2c3.Click += new System.EventHandler(this.l2c3_Click);
            // 
            // l3c1
            // 
            this.l3c1.Font = new System.Drawing.Font("Microsoft YaHei UI", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l3c1.Location = new System.Drawing.Point(127, 340);
            this.l3c1.Name = "l3c1";
            this.l3c1.Size = new System.Drawing.Size(101, 90);
            this.l3c1.TabIndex = 9;
            this.l3c1.Text = "?";
            this.l3c1.UseVisualStyleBackColor = true;
            this.l3c1.Click += new System.EventHandler(this.l3c1_Click);
            // 
            // l3c2
            // 
            this.l3c2.Font = new System.Drawing.Font("Microsoft YaHei UI", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l3c2.Location = new System.Drawing.Point(237, 340);
            this.l3c2.Name = "l3c2";
            this.l3c2.Size = new System.Drawing.Size(101, 90);
            this.l3c2.TabIndex = 10;
            this.l3c2.Text = "?";
            this.l3c2.UseVisualStyleBackColor = true;
            this.l3c2.Click += new System.EventHandler(this.l3c2_Click);
            // 
            // l3c3
            // 
            this.l3c3.Font = new System.Drawing.Font("Microsoft YaHei UI", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l3c3.Location = new System.Drawing.Point(347, 340);
            this.l3c3.Name = "l3c3";
            this.l3c3.Size = new System.Drawing.Size(101, 90);
            this.l3c3.TabIndex = 11;
            this.l3c3.Text = "?";
            this.l3c3.UseVisualStyleBackColor = true;
            this.l3c3.Click += new System.EventHandler(this.l3c3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei UI", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(170, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(240, 46);
            this.label1.TabIndex = 9;
            this.label1.Text = "Jogo do Galo";
            // 
            // player1
            // 
            this.player1.AutoSize = true;
            this.player1.Font = new System.Drawing.Font("Microsoft YaHei UI", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.player1.ForeColor = System.Drawing.Color.White;
            this.player1.Location = new System.Drawing.Point(489, 204);
            this.player1.Name = "player1";
            this.player1.Size = new System.Drawing.Size(160, 46);
            this.player1.TabIndex = 10;
            this.player1.Text = "Player 1:";
            // 
            // player2
            // 
            this.player2.AutoSize = true;
            this.player2.Font = new System.Drawing.Font("Microsoft YaHei UI", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.player2.ForeColor = System.Drawing.Color.White;
            this.player2.Location = new System.Drawing.Point(489, 307);
            this.player2.Name = "player2";
            this.player2.Size = new System.Drawing.Size(160, 46);
            this.player2.TabIndex = 11;
            this.player2.Text = "Player 2:";
            // 
            // score1
            // 
            this.score1.Font = new System.Drawing.Font("Microsoft YaHei UI", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.score1.Location = new System.Drawing.Point(668, 206);
            this.score1.Name = "score1";
            this.score1.ReadOnly = true;
            this.score1.Size = new System.Drawing.Size(89, 44);
            this.score1.TabIndex = 13;
            this.score1.Text = "0";
            this.score1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // score2
            // 
            this.score2.Font = new System.Drawing.Font("Microsoft YaHei UI", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.score2.Location = new System.Drawing.Point(668, 310);
            this.score2.Name = "score2";
            this.score2.ReadOnly = true;
            this.score2.Size = new System.Drawing.Size(89, 44);
            this.score2.TabIndex = 14;
            this.score2.Text = "0";
            this.score2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // resetbtn
            // 
            this.resetbtn.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resetbtn.Location = new System.Drawing.Point(12, 12);
            this.resetbtn.Name = "resetbtn";
            this.resetbtn.Size = new System.Drawing.Size(91, 43);
            this.resetbtn.TabIndex = 1;
            this.resetbtn.Text = "Reset";
            this.resetbtn.UseVisualStyleBackColor = true;
            this.resetbtn.Click += new System.EventHandler(this.resetbtn_Click);
            // 
            // fecharbtn
            // 
            this.fecharbtn.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fecharbtn.Location = new System.Drawing.Point(12, 68);
            this.fecharbtn.Name = "fecharbtn";
            this.fecharbtn.Size = new System.Drawing.Size(91, 43);
            this.fecharbtn.TabIndex = 2;
            this.fecharbtn.Text = "Fechar";
            this.fecharbtn.UseVisualStyleBackColor = true;
            this.fecharbtn.Click += new System.EventHandler(this.fecharbtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(491, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 35);
            this.label3.TabIndex = 17;
            this.label3.Text = "A jogar:";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Microsoft YaHei UI", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.White;
            this.textBox1.Location = new System.Drawing.Point(603, 51);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(115, 35);
            this.textBox1.TabIndex = 12;
            this.textBox1.Text = "Player 1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.fecharbtn);
            this.Controls.Add(this.resetbtn);
            this.Controls.Add(this.score2);
            this.Controls.Add(this.score1);
            this.Controls.Add(this.player2);
            this.Controls.Add(this.player1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.l3c3);
            this.Controls.Add(this.l3c2);
            this.Controls.Add(this.l3c1);
            this.Controls.Add(this.l2c3);
            this.Controls.Add(this.l2c2);
            this.Controls.Add(this.l2c1);
            this.Controls.Add(this.l1c3);
            this.Controls.Add(this.l1c2);
            this.Controls.Add(this.l1c1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(816, 489);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(816, 489);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button l1c1;
        private System.Windows.Forms.Button l1c2;
        private System.Windows.Forms.Button l1c3;
        private System.Windows.Forms.Button l2c1;
        private System.Windows.Forms.Button l2c2;
        private System.Windows.Forms.Button l2c3;
        private System.Windows.Forms.Button l3c1;
        private System.Windows.Forms.Button l3c2;
        private System.Windows.Forms.Button l3c3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label player1;
        private System.Windows.Forms.Label player2;
        private System.Windows.Forms.TextBox score1;
        private System.Windows.Forms.TextBox score2;
        private System.Windows.Forms.Button resetbtn;
        private System.Windows.Forms.Button fecharbtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1;
    }
}

